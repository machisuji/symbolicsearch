package mkai.search.symbolic.t3;

import mkai.search.symbolic.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.*;

import java.util.List;

/**
 * Under construction.
 */
public class Figure1 extends JComponent implements KeyListener, MouseListener {

    public final static List<Point> COORDINATES = Arrays.asList(
            new Point(345, 307), new Point(345, 392), new Point(130, 392), new Point(130, 307), // 1 - 4
            new Point(272, 140), new Point(242, 273), new Point(300, 272), // 5 - 7
            new Point(360, 75), new Point(395, 119), new Point(307, 185), new Point(306, 83), // 8 - 11
            new Point(494, 82), new Point(493, 251), new Point(407, 252), new Point(407, 81), // 12 - 15
            new Point(423, 293), new Point(376, 350), new Point(354, 222), // 16 - 18
            new Point(540, 79), new Point(566, 108), new Point(553, 268), new Point(507, 103) // 19 - 22
    );

    public final static List<Integer> POLYGON_LENGTHS = Arrays.asList(4, 3, 4, 4, 3, 4);

    private SearchTree<Vertex> originalTree;
    private SearchTree<Vertex> tree;

    private int nodeSize = 20;

    private Search<Vertex> search;
    private int goal;

    private final Object lock = new Object();
    private volatile boolean searchRunning = false;

    private volatile SearchTree<Vertex> currentNode;
    private List<SearchTree<Vertex>> options = new LinkedList<SearchTree<Vertex>>();

    private volatile boolean reset = false;
    private volatile boolean setStart = false;
    private volatile boolean setGoal = false;

    public Figure1(SearchTree<Vertex> tree, int goal) {
        this.goal = goal;
        this.search = new AStar<Vertex>(Practical.getDistanceFunction(goal));

        setTree(tree);
    }

    public void setTree(SearchTree<Vertex> tree) {
        this.tree = tree;
        this.originalTree = tree.copy();
        this.currentNode = tree;
        this.options = currentNode.getChildren();
    }

    public JFrame showFigure() {
        JFrame frame = new JFrame("Showing " + tree);

        frame.setContentPane(this);
        frame.setSize(640, 480);
        frame.setResizable(false);
        frame.setLocationByPlatform(true);
        frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

        frame.addMouseListener(this);
        frame.addKeyListener(this);

        frame.setVisible(true);

        return frame;
    }

    @Override
    protected void paintComponent(Graphics graphics) {
        Graphics2D g = (Graphics2D) graphics;

        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.clearRect(0, 0, getWidth(), getHeight());

        g.setColor(new Color(50, 90, 70));
        g.fillRect(0, 0, getWidth(), getHeight());

        paintPolygons(g);
        paintCurrentNode(g);
        paintStart(g);
        paintGoal(g);

        g.setColor(Color.WHITE);
        g.drawString(search.getClass().getSimpleName() + " searching for " + (goal + 1), 50, 50);

        if (goalFound()) {
            String cost = String.format("%.2f", currentNode.getTotalCost());
            g.setColor(Color.MAGENTA);
            g.drawString("Found a path for " + cost, 50, 50 + g.getFont().getSize());
        }

        if (setStart) {
            g.setColor(Color.GREEN);
            g.drawString("CHOOSE START VERTEX", 300, 50);
        }
        if (setGoal) {
            g.setColor(Color.RED);
            g.drawString("CHOOSE GOAL VERTEX", 300, 50);
        }

        int y = 480 - g.getFont().getSize() * 3;
        g.setColor(Color.LIGHT_GRAY);
        g.drawString("[A] - A*", 50, y);
        g.drawString("[B] - Breadth-First", 200, y);
        g.drawString("[G] - Greedy BFS", 350, y);
        g.drawString("[U] - Uniform-Cost", 500, y);

        g.drawString("[Ctrl + S] - Choose start vertex (click)", 50, 20);
        g.drawString("[Ctrl + G] - Choose goal vertex (click)", 350, 20);
    }

    private void paintStart(Graphics2D g) {
        Point pt = COORDINATES.get(getTree().getValue().getIndex());
        g.setColor(Color.GREEN);
        g.fillOval(pt.x - 5, pt.y - 5, 10, 10);
        g.setColor(new Color(50, 50, 50, 190));
        g.drawOval(pt.x - 5, pt.y - 5, 10, 10);
    }

    private void paintGoal(Graphics2D g) {
        Point pt = COORDINATES.get(goal);
        g.setColor(Color.RED);
        g.drawOval(pt.x - 5, pt.y - 5, 10, 10);
    }

    private boolean goalFound() {
        return currentNode != null && currentNode.getValue().getIndex() == goal;
    }

    private void paintCurrentNode(Graphics2D g) {
        if (currentNode != null) {
            int index = currentNode.getValue().getIndex();
            Point pt = COORDINATES.get(index);

            g.setColor(new Color(150, 150, 50, 190));
            for (SearchTree<Vertex> opt : options) {
                Point next = COORDINATES.get(opt.getValue().getIndex());

                g.drawLine(pt.x, pt.y, next.x, next.y);
            }

            g.setColor(Color.YELLOW);
            SearchTree<Vertex> node = currentNode;

            while (node.getParent() != null) {
                Point a = COORDINATES.get(node.getValue().getIndex());
                Point b = COORDINATES.get(node.getParent().getValue().getIndex());

                g.drawLine(a.x, a.y, b.x, b.y);

                node = node.getParent();
            }

            g.setColor(Color.RED);
            g.fillOval(pt.x - 5, pt.y - 5, 10, 10);
            g.setColor(new Color(150, 150, 50, 190));
            g.drawOval(pt.x - 5, pt.y - 5, 10, 10);
        }
    }

    private void paintPolygons(Graphics2D g) {
        g.setColor(Color.WHITE);
        int offset = 0;
        for (int length : POLYGON_LENGTHS) {
            for (int i = 1; i <= length; ++i) {
                Point a = COORDINATES.get(offset + (i - 1) % length);
                Point b = COORDINATES.get(offset + i % length);

                g.drawLine(a.x, a.y, b.x, b.y);
                g.drawString((offset + i) + "", a.x, a.y);
            }
            offset += length;
        }
    }

    protected int getNearestVertex(Point pt) {
        int minIndex = -1;
        int minDist = Integer.MAX_VALUE;

        for (int i = 0; i < COORDINATES.size(); ++i) {
            Point vt = COORDINATES.get(i);

            int dist = (int) Math.sqrt(Math.pow(pt.x - vt.x, 2) + Math.pow(pt.y - vt.y, 2));
            if (dist <= minDist) {
                minIndex = i;
                minDist = dist;
            }
        }

        return minIndex;
    }

    public SearchTree<Vertex> getTree() {
        return tree;
    }

    public void setSearch(Search<Vertex> search) {
        this.search = search;
    }

    public int getNodeSize() {
        return nodeSize;
    }

    public void setNodeSize(int nodeSize) {
        this.nodeSize = nodeSize;
    }

    public void keyTyped(KeyEvent e) {
    }

    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_SPACE) {
            if (searchRunning) {
                synchronized (lock) {
                    lock.notify();
                }
            } else if (!goalFound()) {
                searchRunning = true;

                Thread thread = new Thread(new Runnable() {
                    public void run() {
                        SearchTree<Vertex> g = search.find(new Predicate<Vertex>() {
                            public Boolean apply(Vertex value) {
                                return value.getIndex() == goal;
                            }
                        }, getTree());

                        if (!reset) {
                            currentNode = g;
                            options.clear();
                            repaint();
                        } else {
                            reset = false;
                        }
                        searchRunning = false;
                    }
                });

                getTree().setListener(new SearchTreeListener());

                thread.setDaemon(true);
                thread.start();

                synchronized (lock) {
                    lock.notify();
                }
            }
        }
    }

    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_B) {
            search = new BreadthFirst<Vertex>();
            reset();
        } else if (e.getKeyCode() == KeyEvent.VK_U) {
            search = new UniformCost<Vertex>();
            reset();
        } else if (e.getKeyCode() == KeyEvent.VK_G && !e.isControlDown()) {
            search = new GreedyBestFirst<Vertex>(Practical.getDistanceFunction(goal));
            reset();
        } else if (e.getKeyCode() == KeyEvent.VK_A) {
            search = new AStar<Vertex>(Practical.getDistanceFunction(goal));
            reset();
        } else if (e.getKeyCode() == KeyEvent.VK_S && e.isControlDown()) {
            setStart = !setStart;
            repaint();
        } else if (e.getKeyCode() == KeyEvent.VK_G && e.isControlDown()) {
            setGoal = !setGoal;
            repaint();
        }
    }

    private void reset() {
        currentNode = tree = originalTree.copy();
        options.clear();
        options.addAll(currentNode.getChildren());
        repaint();

        if (searchRunning) {
            reset = true;
            synchronized (lock) {
                lock.notify();
            }
        }
    }

    public void mouseClicked(MouseEvent mouseEvent) {
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
        if (setStart) {
            int vertex = getNearestVertex(e.getPoint());

            setStart = false;
            if (vertex != -1) {
                setTree(Practical.createSearchTree(vertex));
                reset();
            }
        } else if (setGoal) {
            int vertex = getNearestVertex(e.getPoint());

            setGoal = false;
            if (vertex != -1) {
                goal = vertex;
                repaint();
            }
        }
    }

    public void mouseEntered(MouseEvent mouseEvent) {
    }

    public void mouseExited(MouseEvent mouseEvent) {
    }

    class SearchTreeListener implements SearchTree.Listener<Vertex> {
        /**
         * This is called every time a SearchTree node is expanded.
         * After expansion the search is paused to show in the UI
         * which node has just been expanded and which its children are.
         */
        public void onExpansion(SearchTree<Vertex> node, List<SearchTree<Vertex>> children) {
            if (reset) return;

            currentNode = node;
            options.clear();
            options.addAll(children);

            repaint();

            try {
                synchronized (lock) {
                    lock.wait();
                }
            } catch (InterruptedException e) { }
        }
    }
}
