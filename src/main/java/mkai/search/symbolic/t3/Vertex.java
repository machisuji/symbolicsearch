package mkai.search.symbolic.t3;

import mkai.search.symbolic.CountingExpander;

import java.util.LinkedList;
import java.util.List;

public class Vertex {
    private int index;

    Vertex(int index) {
        this.index = index;
    }

    /**
     * The distance from this Vertex to another Vertex.
     */
    public double distanceTo(Vertex vertex) {
        return Practical.distances[this.index][vertex.index];
    }

    /**
     * Checks if a Vertex is visible for this Vertex.
     */
    public boolean isVisible(Vertex vertex) {
        return Practical.visibility[this.index][vertex.index] == 1;
    }

    /**
     * Checks whether this Vertex is visible for another one.
     */
    public boolean isVisibleFor(Vertex vertex) {
        return Practical.visibility[vertex.index][this.index] == 1;
    }

    public int getIndex() {
        return index;
    }

    @Override
    public boolean equals(Object vertex) {
        if (vertex instanceof Vertex) {
            return this.getIndex() == ((Vertex) vertex).getIndex();
        } else {
            return super.equals(vertex);
        }
    }

    @Override
    public String toString() {
        return "Vertex(" + getIndex() + ")";
    }

    public static class Expander extends CountingExpander<Vertex> {
        /**
         * Expands a node, meaning that all nodes whose vertices are visible
         * from that node's vertex are found.
         *
         * @param node The node to be expanded.
         * @return The given node's children.
         */
        public List<Vertex> expandNode(Vertex node) {
            List<Vertex> children = new LinkedList<Vertex>();
            for (int i = 0; i < Practical.visibility.length; ++i) {
                if (Practical.visibility[node.getIndex()][i] == 1 && node.getIndex() != i) {
                    children.add(Practical.vertices.get(i));
                }
            }
            return children;
        }

        public double cost(Vertex from, Vertex to) {
            return Practical.distances[from.getIndex()][to.getIndex()];
        }
    }

    public static class Predicate implements mkai.search.symbolic.Predicate<Vertex> {

        private int goal;

        public Predicate(int goal) {
            this.goal = goal;
        }

        public Boolean apply(Vertex value) {
            return value.getIndex() == goal;
        }
    }
}
