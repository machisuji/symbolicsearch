package mkai.search.symbolic.t3;

import mkai.search.symbolic.SearchTree;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;
import java.util.List;

public class Visualisation {

    final static List<Point> points = Arrays.asList(
            new Point(345, 307), new Point(345, 392), new Point(130, 392), new Point(130, 307), // 1 - 4
            new Point(272, 140), new Point(242, 273), new Point(300, 272), // 5 - 7
            new Point(360, 75), new Point(395, 119), new Point(307, 185), new Point(306, 83), // 8 - 11
            new Point(494, 82), new Point(493, 251), new Point(407, 252), new Point(407, 81), // 12 - 15
            new Point(423, 293), new Point(376, 350), new Point(354, 222), // 16 - 18
            new Point(540, 79), new Point(566, 108), new Point(553, 268), new Point(507, 103) // 19 - 22
    );

    public static void main(String[] args) {
        SearchTree<Vertex> tree = Practical.createSearchTree(2);

        Figure1 fig1 = new Figure1(tree, 18);
        JFrame frame = fig1.showFigure();

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
