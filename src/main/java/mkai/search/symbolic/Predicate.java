package mkai.search.symbolic;

public interface Predicate<V> extends Function<V, Boolean> {
    /**
     * Checks whether this predicate holds for a certain value.
     *
     * @param value The value to be checked.
     * @return True if this predicate holds for the given value.
     */
    Boolean apply(V value);
}
