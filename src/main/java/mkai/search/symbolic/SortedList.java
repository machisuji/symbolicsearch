package mkai.search.symbolic;

import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * A list guaranteed to maintain a given order for all elements.
 * Insertion for this list is in O(n).
 */
public class SortedList<V> extends LinkedList<V> {

    private Comparator<V> comparator;

    /**
     * Creates a new SortedList.
     *
     * @param comparator The Comparator determining the order.
     */
    public SortedList(Comparator<V> comparator) {
        this.comparator = comparator;
    }

    /**
     * Adds a new item to this list at the right position according to this list's order.
     *
     * @param item The item to be inserted into this list.
     * @return true
     */
    @Override
    public boolean add(V item) {
        Iterator<V> entries = this.iterator();
        int i = 0;

        while (entries.hasNext()) {
            V entry = entries.next();

            if (comparator.compare(item, entry) < 0) {
                add(i, item);
                return true;
            } else {
                ++i;
            }
        }

        return super.add(item);
    }
}
