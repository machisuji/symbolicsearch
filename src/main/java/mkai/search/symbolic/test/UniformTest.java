package mkai.search.symbolic.test;

import mkai.search.symbolic.*;

import java.util.*;

/**
 * Test UniformCost search with the example given on Wikipedia.
 * (http://en.wikipedia.org/wiki/Uniform-cost_search)
 */
public class UniformTest {

    private Map<Character, Node> nodes = new HashMap<Character, Node>();
    private char start, end;

    public UniformTest(char start, char end) {
        this.start = start;
        this.end = end;

        for (char name = 'A'; name <= 'G'; ++name) {
            nodes.put(name, new Node(name));
        }
    }

    public static void main(String[] args) {
        UniformTest test = new UniformTest('A', 'G');
        SearchTree<Node> res = test.run(new UniformCost<Node>());

        System.out.println("Found path for " + res.getTotalCost());
        System.out.println("Path: " + res.getRootPathValues());

        List<Node> expectedPath = new LinkedList<Node>();
        for (char name : Arrays.asList('A', 'D', 'F', 'G')) {
            expectedPath.add(test.getNodes().get(name));
        }

        assert res.getRootPathValues().equals(expectedPath);
        assert res.getTotalCost() == 8.0;

        System.out.println("UniformTest passed");
    }

    public SearchTree<Node> run(Search<Node> search) {
        SearchTree<Node> tree = SearchTree.createRoot(nodes.get(start), new NodeExpander());
        Predicate<Node> goal = new Predicate<Node>() {
            public Boolean apply(Node value) {
                return value.name == end;
            }
        };

        return search.find(goal, tree);
    }

    public char getStart() {
        return start;
    }

    public char getEnd() {
        return end;
    }

    public Map<Character, Node> getNodes() {
        return nodes;
    }

    class Node {
        public final char name;

        public Node(char name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "" + name;
        }
    }

    class NodeExpander implements Expander<Node> {
        public List<Node> expand(Node node) {
            switch (node.name) {
                case 'A': return Arrays.asList(nodes.get('D'), nodes.get('B'));
                case 'B': return Arrays.asList(nodes.get('C'));
                case 'C': return Arrays.asList(nodes.get('E'), nodes.get('G'));
                case 'D': return Arrays.asList(nodes.get('E'), nodes.get('F'));
                case 'E': return Arrays.asList(nodes.get('B'));
                case 'F': return Arrays.asList(nodes.get('G'));
                case 'G': return Arrays.asList(nodes.get('E'));
                default: throw new IllegalStateException("This must not happen.");
            }
        }

        public double cost(Node from, Node to) {
            if      (from.name == 'A' && to.name == 'B') return 5;
            else if (from.name == 'A' && to.name == 'D') return 3;
            else if (from.name == 'B' && to.name == 'C') return 1;
            else if (from.name == 'C' && to.name == 'E') return 6;
            else if (from.name == 'C' && to.name == 'G') return 8;
            else if (from.name == 'D' && to.name == 'E') return 2;
            else if (from.name == 'D' && to.name == 'F') return 2;
            else if (from.name == 'E' && to.name == 'B') return 4;
            else if (from.name == 'F' && to.name == 'G') return 3;
            else if (from.name == 'G' && to.name == 'E') return 4;
            else throw new IllegalStateException("This must not happen.");
        }
    }
}
