package mkai.search.symbolic;

import java.util.HashSet;
import java.util.Set;

public class DepthFirst<V> implements Search<V> {

    public SearchTree<V> find(Predicate<V> predicate, SearchTree<V> root) {
        return find(predicate, root, new HashSet<V>());
    }

    public SearchTree<V> find(Predicate<V> predicate, SearchTree<V> root, Set<V> visited) {
        if (predicate.apply(root.getValue())) {
            return root;
        } else if (visited.contains(root.getValue())) {
            return null;
        } else {
            visited.add(root.getValue());
            for (SearchTree<V> child : root.getChildren()) {
                SearchTree<V> goal = find(predicate, child, visited);

                if (goal != null) {
                    return goal;
                }
            }
        }
        return null;
    }
}
