package mkai.search.symbolic;

public interface Function<I, O> {
    O apply(I input);
}
