package mkai.search.symbolic;

import java.util.*;

/**
 * Uniform-Cost Search expands nodes with the lowest past cost first.
 *
 * @param <V> Type of value to be searched.
 */
public class UniformCost<V> implements Search<V> {

    public SearchTree<V> find(Predicate<V> predicate, SearchTree<V> root) {
        Queue<SearchTree<V>> nodes = new SortedList<SearchTree<V>>(getQueueComparator());
        Set<V> visited = new HashSet<V>();

        nodes.add(root);

        while (!nodes.isEmpty()) {
            SearchTree<V> goal = processNext(predicate, nodes, visited);

            if (goal != null) {
                return goal;
            }
        }

        return null;
    }

    /**
     * Tries to find the goal node. If the head of the queue is not the goal
     * the node will be expanded and further explored, that is inserted into the queue.
     *
     * @param goal The predicate designating the goal node.
     * @param queue The queue of nodes to be processed.
     * @param visited A set of already visited nodes which should not be visited again.
     * @return A SearchTree if the queue's head is the goal. Null otherwise.
     */
    protected SearchTree<V> processNext(Predicate<V> goal, Queue<SearchTree<V>> queue, Set<V> visited) {
        SearchTree<V> node = queue.poll();

        if (goal.apply(node.getValue())) {
            return node;
        } else {
            visited.add(node.getValue());

            for (SearchTree<V> child : node.getChildren()) {
                if (!visited.contains(child.getValue())) {
                    insert(child, queue);
                }
            }
        }

        return null;
    }

    /**
     * Comparator for sorting SearchTree nodes after their total cost (ascending).
     *
     * @see #compareNodes
     */
    protected Comparator<SearchTree<V>> getQueueComparator() {
        return new Comparator<SearchTree<V>>() {
            public int compare(SearchTree<V> a, SearchTree<V> b) {
                return compareNodes(a, b);
            }
        };
    }

    /**
     * Inserts a SearchTree node into a Queue if there is no other node with the
     * same value but a lower cost.
     *
     * @param node Node to be inserted into the Queue.
     * @param nodes Queue into which the node is to be inserted.
     * @return True if the node was inserted, false otherwise.
     * @see #compareNodes
     */
    protected boolean insert(SearchTree<V> node, Queue<SearchTree<V>> nodes) {
        Iterator<SearchTree<V>> i = nodes.iterator();
        boolean replace = false;
        boolean found = false;

        while (i.hasNext()) {
            SearchTree<V> e = i.next();

            if (e.getValue().equals(node.getValue())) {
                if (!found) {
                    found = true;
                }
                if (compareNodes(e, node) > 0) {
                    replace = true;
                    i.remove();
                }
            }
        }

        if (replace || !found) {
            nodes.add(node);

            return true;
        } else {
            return false;
        }
    }

    /**
     * Determines the ordering of the to-be-processed nodes based on the nodes' total cost.
     *
     * @param a A node.
     * @param b Another node.
     * @return The difference in cost between the two nodes.
     */
    protected int compareNodes(SearchTree<V> a, SearchTree<V> b) {
        return (int) Math.round(a.getTotalCost() - b.getTotalCost());
    }
}
