package mkai.search.symbolic;

import java.util.Queue;
import java.util.Set;

/**
 * Greedy BFS expands nodes with the lowest estimated future cost first.
 *
 * @param <V> Type of node to be searched.
 */
public class GreedyBestFirst<V> extends UniformCost<V> {

    private Function<V, Double> targetDistance;

    public GreedyBestFirst(Function<V, Double> targetDistance) {
        this.targetDistance = targetDistance;
    }

    @Override
    protected SearchTree<V> processNext(Predicate<V> goal, Queue<SearchTree<V>> queue, Set<V> visited) {
        SearchTree<V> next = queue.peek();

        if (goal.apply(next.getValue())) {
            return next;
        }

        for (SearchTree<V> succ : next.getChildren()) {
            if (!visited.contains(succ.getValue())) {
                if (compareNodes(succ, next) < 0) {
                    insert(succ, queue);

                    return null; // succ will be inserted before parent and checked next
                } else {
                    insert(succ, queue);
                }

            }
        }

        visited.add(queue.remove().getValue());

        return null;
    }

    /**
     * Determines the ordering of the to-be-processed nodes based on their estimated future cost.
     *
     * @param a A node.
     * @param b Another node.
     * @return The difference in cost between the two nodes.
     */
    @Override
    protected int compareNodes(SearchTree<V> a, SearchTree<V> b) {
        return (int) Math.round(getTargetDistance().apply(a.getValue()) - getTargetDistance().apply(b.getValue()));
    }

    /**
     * The heuristic used to estimate the future cost of a node.
     *
     * @return A function calculating the future cost of a given node.
     */
    public Function<V, Double> getTargetDistance() {
        return targetDistance;
    }
}
