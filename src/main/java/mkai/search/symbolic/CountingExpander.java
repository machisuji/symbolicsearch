package mkai.search.symbolic;

import java.util.List;

public abstract class CountingExpander<V> implements Expander<V> {

    private int expandedNodes;

    abstract public List<V> expandNode(V node);

    public List<V> expand(V node) {
        List<V> nodes = expandNode(node);
        expandedNodes += nodes.size();

        return nodes;
    }

    public int getExpandedNodes() {
        return expandedNodes;
    }

    @Override
    public String toString() {
        return String.format("CountingExpander(nodes = %d)", getExpandedNodes());
    }
}
