package mkai.search.symbolic;

public interface Search<V> {
    /**
     * Tries to find a value for which a certain predicate holds in a SearchTree on the shortest path.
     *
     * @param predicate The predicate required to be true for the result.
     * @param root The tree in which to search for a value.
     * @return A SearchTree node if one whose value matches could be found, null otherwise.
     */
    SearchTree<V> find(Predicate<V> predicate, SearchTree<V> root);
}
