package mkai.search.symbolic;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class SearchTree<V> {

    private SearchTree<V> parent;
    private List<SearchTree<V>> children = new LinkedList<SearchTree<V>>();

    private V value;
    private double totalCost;

    private Expander<V> expander;
    private boolean expanded;

    private Listener listener;

    /**
     * Creates a new SearchTree node.
     *
     * @param parent
     * @param value
     */
    SearchTree(SearchTree<V> parent, V value) {
        this.parent = parent;
        this.value = value;
    }

    /**
     * Creates a new SearchTree root node.
     *
     * @param value
     */
    SearchTree(V value, Expander<V> expander) {
        this.value = value;
        this.expander = expander;
    }

    public static <V> SearchTree<V> createRoot(V value, Expander<V> expander) {
        return new SearchTree<V>(value, expander);
    }

    public static <V> SearchTree<V> createNode(SearchTree<V> parent, V value) {
        return new SearchTree<V>(parent, value);
    }

    public SearchTree<V> copy() {
        SearchTree<V> copy = new SearchTree<V>(getParent(), getValue());
        copy.setExpander(getExpander());

        return copy;
    }

    /**
     * Expands this SearchTree node.
     *
     * @return The number of children after the expansion or -1 if it the node was already expanded.
     */
    protected int expand() {
        if (!isExpanded()) {
            List<V> values = getExpander().expand(getValue());
            for (V value : values) {
                SearchTree<V> child = createNode(this, value);

                child.setExpander(getExpander());
                child.setListener(getListener());
                child.setTotalCost(getTotalCost() + getExpander().cost(getValue(), child.getValue()));

                children.add(child);
            }
            setExpanded(true);

            if (getListener() != null) {
                getListener().onExpansion(this, Collections.unmodifiableList(children));
            }

            return values.size();
        } else {
            return -1;
        }
    }

    public V getValue() {
        return value;
    }

    public SearchTree<V> getParent() {
        return parent;
    }

    protected void setParent(SearchTree<V> parent) {
        this.parent = parent;
    }

    public List<SearchTree<V>> getChildren() {
        expand();
        return children;
    }

    public void setExpander(Expander<V> expander) {
        this.expander = expander;
    }

    public Expander<V> getExpander() {
        return expander;
    }

    public List<SearchTree<V>> getRootPath() {
        List<SearchTree<V>> path = new LinkedList<SearchTree<V>>();
        buildRootPath(path);

        return path;
    }

    protected void buildRootPath(List<SearchTree<V>> path) {
        if (!isRoot()) {
            getParent().buildRootPath(path);
        }
        path.add(this);
    }

    public List<V> getRootPathValues() {
        List<V> values = new LinkedList<V>();
        buildRootPathValues(values);

        return values;
    }

    protected void buildRootPathValues(List<V> values) {
        if (!isRoot()) {
            getParent().buildRootPathValues(values);
        }
        values.add(getValue());
    }

    public boolean isRoot() {
        return parent == null;
    }

    public boolean isLeaf() {
        return getChildren().isEmpty();
    }

    public boolean isExpanded() {
        return expanded;
    }

    protected void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }

    /**
     * The total cost from root.
     */
    public double getTotalCost() {
        return totalCost;
    }

    protected void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }

    @Override
    public String toString() {
        String type = "node";
        if (isLeaf()) {
            type = "leaf";
        } else if (isRoot()) {
            type = "root";
        }
        return String.format("SearchTree<%s>(%s, %s)",
                getValue().getClass().getSimpleName(), getValue(), type);
    }

    public Listener getListener() {
        return listener;
    }

    public void setListener(Listener listener) {
        this.listener = listener;

        if (isExpanded()) {
            for (SearchTree<V> child : getChildren()) {
                child.setListener(listener);
            }
        }
    }

    public interface Listener<V> {
        void onExpansion(SearchTree<V> node, List<SearchTree<V>> children);
    }
}
