package mkai.search.symbolic;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class BreadthFirst<V> implements Search<V> {
    public SearchTree<V> find(Predicate<V> predicate, SearchTree<V> root) {
        Queue<SearchTree<V>> nodes = new LinkedList<SearchTree<V>>(Arrays.asList(root));

        while (!nodes.isEmpty()) {
            SearchTree<V> head = nodes.poll();

            if (predicate.apply(head.getValue())) {
                return head;
            } else {
                nodes.addAll(head.getChildren());
            }
        }
        return null;
    }
}
