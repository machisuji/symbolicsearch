package mkai.search.symbolic;

import java.util.List;

/**
 * An Expander defines the parent-child relationship between values.
 * Additionally it defines the path cost from one value to another.
 *
 * @param <V> The type of values to be associated.
 */
public interface Expander<V> {
    /**
     * Discover the children to a given value.
     *
     * @param node A value whose children are to be found.
     * @return A list with all children of the given value.
     */
    List<V> expand(V node);

    /**
     * Calculates the cost for moving from one node to an (adjacent) other.
     *
     * @param from Start node
     * @param to End node
     * @return The cost between the two given nodes.
     */
    double cost(V from, V to);
}
