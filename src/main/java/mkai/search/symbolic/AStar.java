package mkai.search.symbolic;

/**
 * A* simply combines Greedy BFS and Uniform-cost search.
 * That is it always goes for the node where both total cost
 * and goal distance are as low as possible.
 *
 * @param <V> The type of value to be searched.
 */
public class AStar<V> extends GreedyBestFirst<V> {

    private final UniformCost<V> uniformCost = new UniformCost<V>();

    public AStar(Function targetDistance) {
        super(targetDistance);
    }

    @Override
    protected int compareNodes(SearchTree<V> a, SearchTree<V> b) {
        return super.compareNodes(a, b) + uniformCost.compareNodes(a, b);
    }
}
